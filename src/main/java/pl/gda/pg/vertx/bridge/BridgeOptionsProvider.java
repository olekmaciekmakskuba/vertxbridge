package pl.gda.pg.vertx.bridge;

import io.vertx.ext.apex.handler.sockjs.BridgeOptions;
import io.vertx.ext.apex.handler.sockjs.PermittedOptions;

import java.util.Arrays;

public class BridgeOptionsProvider {

	private BridgeOptions options;

	public BridgeOptionsProvider() {
		this.options = new BridgeOptions();

		Arrays.asList(VertxServices.values())
			  .stream()
			  .map(VertxServices::getAddress)
			  .map(this::cretePermittedOptions)
			  .forEach(options::addInboundPermitted);
	}

	private PermittedOptions cretePermittedOptions(String address) {
		return new PermittedOptions().setAddress(address);
	}

	public BridgeOptions get() {
		return options;
	}
}
