package pl.gda.pg.vertx.bridge;

public enum VertxServices {
	COMPILER("compile.project");

	private final String address;

	VertxServices(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}
}
