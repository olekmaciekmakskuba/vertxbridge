package pl.gda.pg.vertx.bridge;

import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.apex.Router;
import io.vertx.ext.apex.handler.CorsHandler;
import io.vertx.ext.apex.handler.sockjs.BridgeOptions;
import io.vertx.ext.apex.handler.sockjs.SockJSHandler;

public class EventBusBridgeEntryPoint {

	public static void main(String[] args) {
		EventBusBridgeEntryPoint eventBusBridgeEntryPoint = new EventBusBridgeEntryPoint();
		eventBusBridgeEntryPoint.start();
	}

	private void start() {
		VertxOptions options = new VertxOptions();
		options.setClusterHost("localhost");

		Vertx.clusteredVertx(options, this::createBridge);
	}

	private void createBridge(AsyncResult<Vertx> result) {
		Vertx vertx = result.result();

		Router router = Router.router(vertx);
		router.route().handler(CorsHandler.create("*").allowedMethod(HttpMethod.GET));

		SockJSHandler sockJSHandler = createSockJSHandler(vertx);
		router.route("/eventbus/*").handler(sockJSHandler);

		vertx.createHttpServer().requestHandler(router::accept).listen(8081);
	}

	private SockJSHandler createSockJSHandler(Vertx vertx) {
		SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
		BridgeOptions options = new BridgeOptionsProvider().get();
		sockJSHandler.bridge(options);
		return sockJSHandler;
	}
}
